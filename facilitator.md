# Facilitator Notes #
This is a simple CTF challenge for Kids Securiday to introduce the idea of source code review and
the recon stage of penetration testing. Alternatively: it's an introduction to coding and secure 
development.

If you're a participant and you found this, well played!

If you're a facilitator and wondering why all the screenshots are of starwars things - this is a pretty simple C-f/C-r on the previous themed challenge.

## Scenatio ##:
Mega Corp has stolen the only copy of Canberran Fried Chicken's (CFC's) 8 secret flavors and condiments recipe. With out this _special sauce_ they won't be able to launch at their grand opening!

CFC thinks the recipe has been stored in Mega Corp's Secret Sauce Page (https://tsujamin.gitlab.io/maythesaucebewithyou-ctf-challenge). See what you can find!

## Flags ##
The flags and walkthroughs follow:


### Flag 1 ###
*Name:* May the Sauce be with you

*Description:* Find the flag Mega Corp left in their Secure Sauce website: https://tsujamin.gitlab.io/maythesaucebewithyou-ctf-challenge/

*Flag:* `FLAG{may_THE_sauce_be_WITH_u}`

*Location:* [index.html](public/index.html)

*Hint 1*: "Source Code" is the name of the raw version of a program or website

*Hint 2*: View the source Luke

*Hint 3:* Right click on the website and click "View Source", the Flag should be visible.

*Walkthrough*
Participants should navigate to the website 
(https://tsujamin.gitlab.io/maythesaucebewithyou-ctf-challenge/) and be greeted wih a 
spinning sauce bottle, and a comment about source code and security.

![Homepage](screenshots/Screen Shot 2020-01-15 at 8.38.37 pm.png)

The words "source code" link to a WikiHow article on how to view the source code of a 
website. This should lead participants down the track of viewing the source code of the 
homepage.

![Wikihow](screenshots/Screen Shot 2020-01-15 at 8.39.18 pm.png)

Once participants views the source of the homepage, they should see some comments about 
HTML and leaving sensitive information in comments/source code. The flag will be at the
bottom of the source.

![Source](screenshots/Screen Shot 2020-01-15 at 9.06.23 pm.png)


### Flag 2 ###
*Name:* Search harder, you must...

*Description:* Mega Corp has left more secrets in their Secure Sauce project, try and find where they store them!

*Flag:* `FLAG{h0t_AND_sp1cy_S3CR3TS}`

*Location:* [README.md](README.md)

*Hing 1*: Finding where the code is stored can be valuable...

*Hint 2*: Developers can accidentally reveal too much information about themselves in their work

*Hint 3*: Check out the link to the developers projects at the bottom of the homepage!

This flag is to guide participants to look further in their investigations, and to introduce 
the concept of repositories vs websites. At the bottom of the homepage is a link to "Mega Corp's"
Gitlab profile, containing this repository.

![Homepage](screenshots/Screen Shot 2020-01-15 at 9.05.34 pm.png)

![Gitlab](screenshots/Screen Shot 2020-01-15 at 9.05.41 pm.png)

Once the participant opens the project, the flag is in the README.md shown on the project 
homepage.

![README](screenshots/Screen Shot 2020-01-15 at 9.05.52 pm.png)

### Flag 3 ###
*Name:* It's a flag!

*Description:* You've found the secret project files! Find the hidden flag. 

*Flag:* `FLAG{t@sty_API_key_MmMmMmMm}`

*Location:* [saucey-secrets.yaml](config/saucey-secrets.yaml)

*Hint 1*: Code projects often have secrets that don't end up in the final version

*Hint 2*: SEARCH harder!

*Hint 3:* What do all the FLAG{ have in common?

Once the participant is on the repo, they should be able to search for the final flag using 
Gitlab's search. Searching either "secret" or "FLAG" should be sufficient.      

![Search](screenshots/Screen Shot 2020-01-15 at 9.11.00 pm.png)

![Results](screenshots/Screen Shot 2020-01-15 at 9.11.25 pm.png)

The participant might stumble upon this guide (hello if that is you!) but that's still a fair way
to win. 

### Flag 4 ###
*Name:* 1980's Crypto, 2020's flavour

*Description*: The saucey-secrets file contained the stolen recipe, but it's encrypted! Surely no one could crack a 1 byte key...

*Flag:* Not this time sorry!

*Location:* [saucey-secrets.yaml](config/saucey-secrets.yaml) encrypted w/ 1 byte XOR key

*Hint 1*: A common type of encryption is called XOR, but make sure to Base64 decode!

*Hint 2:* There are only 256 possible 1 byte XOR keys (0-255, or 0x00-0xFF)

*Hint 3:* Small encryption keys can be cracked by Brute Force! https://gchq.github.io/CyberChef/#recipe=From_Base64('A-Za-z0-9%2B/%3D',true)XOR_Brute_Force(1,100,0,'Standard',false,true,false,'')&input=UFVUX1NFQ1JFVF9IRVJF

