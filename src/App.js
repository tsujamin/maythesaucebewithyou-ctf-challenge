import React from 'react';
import logo from './logo.png';
import './App.css';
import StickyFooter from 'react-sticky-footer';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Welcome to the Mega Corp Secure Sauce Page. 
        </p>
        <p>
        We're still working on the <a style={{color: "#808080"}} href="https://www.wikihow.com/View-Source-Code">source code</a> at the moment as we learn how to secure <i>our</i> 8 secret flavors and condiment recipe from the competition!
        </p>
      </header>
      <div className="App-header">
        <StickyFooter>
          Made with <span role="img">❤️</span> by <a style={{color: "#808080"}} href="https://gitlab.com/users/tsujamin/projects">Mega Corp</a> and Colonel @tsujamin
        </StickyFooter> 
      </div>
    </div>
  );
}

export default App;
